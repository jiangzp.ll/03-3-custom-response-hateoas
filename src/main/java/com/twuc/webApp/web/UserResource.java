package com.twuc.webApp.web;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

class UserResource extends Resource {
    public UserResource(User user, Link... links) {
        super(user, links);
    }
}
